const express = require('express')
const mongoose = require('mongoose');
const Product = require('./models/product.model.js');
const Customer = require('./models/customer.model.js');
const Factory = require('./models/factory.model.js');
const Temperature = require('./models/temperature.model.js');
const {getProducts,getProduct,userByProduct,updateProduct,deleteProduct} = require('./controllers/product.controller.js');
const {addCustomer,findCustomers,findCustomer,updateCustomer} = require('./controllers/customer.controller.js');
const {create_factory,update_factory} = require('./controllers/factory.controller.js');
const {setTemperature,getTemperature,getHeighTemperature,getLowTemperature} = require('./controllers/temperature.controller.js');
const { Double } = require('mongodb');
const app = express()
app.use(express.json());
app.use(express.urlencoded({extended:false}));

app.listen(3000, () => {
})

//products
app.use('/api/get_products_all' ,getProducts );
app.use('/api/get_product' ,getProduct);
app.use('/api/get_product_user/',userByProduct);
app.use('/api/update_product/:id',updateProduct);
app.use('/api/delete_product/',deleteProduct);


//customer
app.use('/api/add_customer' , addCustomer);//issue
app.use('/api/get_customer',findCustomer);
app.use('/api/find_customer_all' , findCustomers);
app.use('/api/update_customer' ,updateCustomer);



//factory
app.use('/api/add_factory' ,create_factory);
app.use('/api/update_factory' ,update_factory);

//temperature
app.use('/api/temp' , setTemperature);
app.use('/api/get_tmp',getTemperature );
app.use('/api/highTemp',getHeighTemperature);
app.use('/api/lowTemp',getLowTemperature);







//mongodb+srv://admin:<mhrxWfh27zxre4O1>@backenddb.bw0vchk.mongodb.net/?retryWrites=true&w=majority&appName=BackendDB
mongoose.connect("mongodb+srv://admin:sByuk0sBrUwg0WVf@backenddb.6onwxw4.mongodb.net/test_project_one?retryWrites=true&w=majority&appName=backenddb")
    .then(() => {

        // res.send("Connected...!"
        console.log("Connected...!")
    })
    .catch(
        console.log("Connection failed..!")

    );


