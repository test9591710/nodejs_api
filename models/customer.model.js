const mongoose = require('mongoose');

const customerSchema = mongoose.Schema(
    {
        customer_name: {
            type: String,
            require: [true, "Customer name is required"],
        },
        customer_mobile: {
            type: Number,
            require: [true, "Customer contact number is required"],
            unique:[true]
           
        },
        customer_points: {
            type: String,
            require: [false],
            default: 0
        },
        customer_address: {
            type: String,
            require: [false],
            default: 0
        }
    },{timestamp:true}

);
const Customer = mongoose.model("customer" ,customerSchema );
module.exports = Customer;
// const Product = mongoose.model("Product" , ProductSchema);
// module.exports = Product;