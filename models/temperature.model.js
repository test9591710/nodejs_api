const mongoose = require('mongoose');
const TemperatureSchema = mongoose.Schema(
    {
        celsius:{
            type:Number
        },
        fahrenheit:{
            type:Number
        },
        humidity:{

        },
        location:{
            type:String
        },
        condition:{
            type:String,
           
        }

    },
    {
        timestamps:true
    }
);

const Temperature = mongoose.model("Temperature" , TemperatureSchema);
module.exports = Temperature;
