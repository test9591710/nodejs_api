const { Timestamp } = require('mongodb');
const mongoose = require('mongoose');

const FactorySchema = mongoose.Schema({
    location:{
        type:String,
        require:[true]
    },
    product:{
        type:String,
        requires:[true],
      
    },
    currency:{
        type:String,
        require:[false]

    }
},
   { Timestamp:true}
);

const Factory = mongoose.model("Factory" , FactorySchema);
module.exports = Factory;


// const Product = mongoose.model("Product" , ProductSchema);
// module.exports = Product;