const express = require('express');
const factory = require('../models/factory.model');
const router = express.router();
const {create_factory,update_factory}= require('../controllers/factory.controller');

router.post('/',create_factory);
router.put('/',update_factory);
