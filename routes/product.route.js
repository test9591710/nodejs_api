const express = require("express");
const Product = require("../models/product.model");
const router = express.Router();
const {getProducts,getProduct ,userByProduct,updateProduct,deleteProduct} = require('../controllers/product.controller');


router.get('/' ,getProducts);
router.post('/' ,getProduct );
router.post('/' , userByProduct);
router.put('/:id' , updateProduct);
router.delete('/' ,deleteProduct);