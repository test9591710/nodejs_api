const express = require('express');
const customer = require('../models/customer.model')
const router = express.Router();
const {addCustomer , findCustomers ,findCustomer,updateCustomer} = require('../controllers/customer.controller');


router.post('/' , addCustomer);
router.get('/' ,findCustomer);
router.get('/' ,findCustomers);
router.put('/' ,updateCustomer)