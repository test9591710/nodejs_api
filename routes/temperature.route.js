const express = require('express');
const temperature = require('../models/temperature.model');
const router = express.Router();
const {setTemperature,getTemperature,getHeighTemperature,getLowTemperature} = require('../controllers/temperature.controller');

router.post('/' , setTemperature);
router.get('/',getTemperature );
router.post('/',getHeighTemperature);
router.post('/' , getLowTemperature);