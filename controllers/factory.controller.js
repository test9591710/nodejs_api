const { models } = require('mongoose');
const factory = require('../models/factory.model');

const create_factory = async (req, res)=>{
    try {
        const factory = await Factory.create(req.body);
        res.status(200).json(factory);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

const update_factory = async (req,res)=>{
    try {
        const { id } = req.body;
        const factory_update = await Factory.findByIdAndUpdate(id, req.body);
        if (!factory_update) {
            res.status(200).json({ "message": "Factory not found..!" });
        }
        const updated_factory = await Factory.findById(id);
        res.status(200).json({ "message": "updated location is " + updated_factory.location });
    } catch (error) {
        res.status(500).json({ "message": error.message });
    }
}

module.exports = {
    create_factory,
    update_factory
}