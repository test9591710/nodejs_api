const Customer = require('../models/customer.model');


const addCustomer = async (req, res) => {
    try {
        const customer = await Customer.create(req.body);
        // res.status(200).json(customer);
        res.status(200).json({ "Message": "Customer add successfully..!" })
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}
const findCustomer = async (req, res) => {
    try {
        const { id } = req.body;
        const customer = await Customer.findById(id);
        if (!customer) {
            res.status(200).json({ "message": "Customer Not Found..!" });
        }
        res.status(200).json(customer);
    } catch (error) {
        res.status(500).json({ "message": error.message });
    }
}

const findCustomers = async (req, res) => {
    try {
        // const { id } = req.body;
        const customer_all = await Customer.find({});
        res.status(200).json(customer_all);
    } catch (error) {
        res.status(500).json({ "message": error.message });
    }
}

const updateCustomer = async(req,res)=>{

}

module.exports = {
    addCustomer,
    findCustomers,
    findCustomer,
    updateCustomer
}