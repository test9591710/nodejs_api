const Product = require('../models/product.model');


const getProducts = async (req, res) => {
    try {
        const products = await Product.find({});
        res.status(200).json(products);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}
const getProduct = async (req, res) => {
    try {
        const { id } = req.body;
        const product = await Product.findById(id);
        res.status(200).json(product);
    } catch (error) {
        res.status(500).json({ "message": error.message });
    }
}

const userByProduct = async (req, res) => {
    try {
        // const product_id = await Product.create(req.body)
        const { id } = req.body;
        const product = await Product.findById(id);
        if (!product) {
            res.status(200).json({ "message": "Product not found" });
        }
        res.status(200).json({ "product_name": product.name });
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

const updateProduct = async (req, res) => {
    try {
        const { id } = req.params;
        const product = await Product.findByIdAndUpdate(id, req.body);
        if (!product) {
            return res.status(404).json({ "message": "Product not found" });
        }
        if (product.name == req.body.name) {
            res.status(200).json({ "message": "Product name already exist" });
        } else {
            const updatedProduct = await Product.findById(id);
            res.status(200).json(updatedProduct);
        }
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}
const deleteProduct = async (req, res) => {
    try {
        // const {id} = req.params;
        const { id } = req.body;
        const product = await Product.findByIdAndDelete(id);

        if (!product) {
            return res.status(404).json({ "message ": "product not found" });
        }

        res.status(200).json({ "message": "Product has been deleted..!" });

    } catch (error) {
        res.status(500).json({ "message": error.message });
    }

}
module.exports = {
    getProducts,
    getProduct,
    userByProduct,
    updateProduct,
    deleteProduct
}